# CloudJumper
Bouncing game in which the aim is to rise higher and higher.
</br></br>
You can play both on PC and on Smartphone (better experience on PC). On Smartphone you need to use a browser and not a HTML viewer.
<br/><br/>
Try it online at https://drill.gitlab.io/HTML5_Games-CloudJumper
</br></br>

## Commands
There are 3 types of commands:
<ul>
  <li><b>Keyboard</b>: use the horizontal directional arrows to move the sprite right or left.</li>
  <li><b>Mouse</b>: move the cursor to control the sprite. If you have a touchscreen, you can use your finger.</li>
  <li><b>Buttons</b>: click/tap the buttons to move the sprite left or right (better for touchscreen devices).</li>
</ul>
</br>

## Screenshots
![CloudJumper_screenshot1](https://gitlab.com/Drill/HTML5_Games-CloudJumper/uploads/6c9b1e0269e9fdc89247e658bd456906/CloudJumper_screenshot1.PNG)
![CloudJumper_screenshot2](https://gitlab.com/Drill/HTML5_Games-CloudJumper/uploads/05e945b7932865376e22e754508e3c8c/CloudJumper_screenshot2.PNG)
<br/><br/>

## Credits
Check out the `.html` file to see credits.
<br/><br/>

---


<i>Note</i>: if you change the files position, you'll need to update the relative paths in the `.html` file.
